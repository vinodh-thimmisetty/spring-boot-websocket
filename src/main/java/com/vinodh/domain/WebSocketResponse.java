package com.vinodh.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WebSocketResponse {

    private String content;
}
