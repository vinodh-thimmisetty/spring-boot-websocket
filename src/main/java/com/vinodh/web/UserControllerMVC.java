package com.vinodh.web;

import com.vinodh.config.events.EventPublisher;
import com.vinodh.domain.User;
import com.vinodh.domain.WebSocketResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserControllerMVC {

    private EventPublisher eventPublisher;

    public UserControllerMVC(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @GetMapping("/user")
    public ResponseEntity<WebSocketResponse> home(User user) {
        eventPublisher.publishEvent(user);
        return ResponseEntity.ok(WebSocketResponse.builder().content("MVC ~~ User Details::" + user.getName() + " | " + user.getEmail()).build());
    }

}
