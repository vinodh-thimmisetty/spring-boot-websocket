package com.vinodh.web.socket;

import com.vinodh.domain.User;
import com.vinodh.domain.WebSocketResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class UserControllerSocket {

    @MessageMapping("/user")
    @SendTo("/topic/user")
    public WebSocketResponse home(User user) {
        return WebSocketResponse.builder().content("WS ~~ User Details::" + user.getName() + " | " + user.getEmail()).build();
    }

    @SubscribeMapping("/load")
    public WebSocketResponse load() {
        return WebSocketResponse.builder().content("SubscribeMapping ~~ User Details:: Vinodh Kumar | yoyo").build();

    }

}
