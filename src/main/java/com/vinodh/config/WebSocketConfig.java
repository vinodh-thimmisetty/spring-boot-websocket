package com.vinodh.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Slf4j
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        log.info("Registering the STOMP End points");
        registry.addEndpoint("/vinodh")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        log.info("Configure In memory Message Broker");
        // All messages will be passed to Broker before returning back to UI
        registry.enableSimpleBroker("/topic", "/queue"); // queue (One-One); topic (One-Many)
        // Forwards to Controller to find @RequestMapping("/app")
        registry.setApplicationDestinationPrefixes("/app");

    }
}
