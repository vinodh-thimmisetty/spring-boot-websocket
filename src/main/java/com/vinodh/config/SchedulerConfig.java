package com.vinodh.config;

import com.vinodh.domain.WebSocketResponse;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Configuration
@EnableScheduling
public class SchedulerConfig {

    private SimpMessagingTemplate simpMessagingTemplate;


    public SchedulerConfig(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }


    @Scheduled(fixedDelay = 10000)
    public void sample() {
        simpMessagingTemplate.convertAndSend("/topic/user", WebSocketResponse.builder().content("Scheduler::" + LocalDateTime.now()).build());
    }

}
