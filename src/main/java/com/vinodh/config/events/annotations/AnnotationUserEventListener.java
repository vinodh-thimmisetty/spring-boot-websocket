package com.vinodh.config.events.annotations;

import com.vinodh.config.events.custom_events.UserEvent;
import com.vinodh.domain.WebSocketResponse;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class AnnotationUserEventListener {
    private SimpMessagingTemplate simpMessagingTemplate;

    public AnnotationUserEventListener(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @EventListener
    public void handle(UserEvent userEvent) {
        simpMessagingTemplate.convertAndSend("/topic/user", WebSocketResponse.builder().content("Annotation based Application Event Listener::" + LocalDateTime.now() + userEvent.getUser().getName() + " | " + userEvent.getUser().getEmail()).build());
    }
}
