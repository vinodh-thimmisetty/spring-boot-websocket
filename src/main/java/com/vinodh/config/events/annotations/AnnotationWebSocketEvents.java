package com.vinodh.config.events.annotations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.*;

@Slf4j
@Component
public class AnnotationWebSocketEvents {

    private SimpMessagingTemplate simpMessagingTemplate;

    public AnnotationWebSocketEvents(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @EventListener
    public void handle(ApplicationEvent applicationEvent) {
        if (applicationEvent instanceof SessionConnectEvent) {
            log.info("Annotation WebSocket Connect Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "Annotation  WebSocket Connect Event");
        } else if (applicationEvent instanceof SessionConnectedEvent) {
            log.info("Annotation WebSocket Connected Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "Annotation WebSocket Connected Event");
        } else if (applicationEvent instanceof SessionDisconnectEvent) {
            log.info("Annotation WebSocket Disconnect Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "Annotation WebSocket Disconnect Event");
        } else if (applicationEvent instanceof SessionSubscribeEvent) {
            log.info("Annotation WebSocket Subscribe Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "Annotation WebSocket Subscribe Event");
        } else if (applicationEvent instanceof SessionUnsubscribeEvent) {
            log.info("Annotation WebSocket Unsubscribe Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "Annotation WebSocket Unsubscribe Event");
        }
    }
}
