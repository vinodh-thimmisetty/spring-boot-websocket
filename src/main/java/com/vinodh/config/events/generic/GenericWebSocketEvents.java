package com.vinodh.config.events.generic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.*;

@Slf4j
@Component
public class GenericWebSocketEvents implements ApplicationListener<ApplicationEvent> {

    private SimpMessagingTemplate simpMessagingTemplate;

    public GenericWebSocketEvents(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if (applicationEvent instanceof SessionConnectEvent) {
            log.info("WebSocket Connect Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "WebSocket Connect Event");
        } else if (applicationEvent instanceof SessionConnectedEvent) {
            log.info("WebSocket Connected Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "WebSocket Connected Event");
        } else if (applicationEvent instanceof SessionDisconnectEvent) {
            log.info("WebSocket Disconnect Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "WebSocket Disconnect Event");
        } else if (applicationEvent instanceof SessionSubscribeEvent) {
            log.info("WebSocket Subscribe Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "WebSocket Subscribe Event");
        } else if (applicationEvent instanceof SessionUnsubscribeEvent) {
            log.info("WebSocket Unsubscribe Event");
            simpMessagingTemplate.convertAndSend("/topic/user", "WebSocket Unsubscribe Event");
        }
    }
}
