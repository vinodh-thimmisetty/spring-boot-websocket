package com.vinodh.config.events.generic;

import com.vinodh.config.events.custom_events.UserEvent;
import com.vinodh.domain.WebSocketResponse;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class GenericUserEventListener implements ApplicationListener<UserEvent> {

    private SimpMessagingTemplate simpMessagingTemplate;

    public GenericUserEventListener(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(UserEvent userEvent) {
        simpMessagingTemplate.convertAndSend("/topic/user", WebSocketResponse.builder().content("Generic Application Event Listener::" + LocalDateTime.now() + userEvent.getUser().getName() + " | " + userEvent.getUser().getEmail()).build());
    }
}
