package com.vinodh.config.events;

import com.vinodh.config.events.custom_events.UserEvent;
import com.vinodh.domain.User;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EventPublisher {

    private ApplicationEventPublisher applicationEventPublisher;

    public EventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publishEvent(User user) {
        applicationEventPublisher.publishEvent(new UserEvent("User Event is Triggered !!", user));
    }

}
